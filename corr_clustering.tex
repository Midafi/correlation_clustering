\documentclass[10pt,a4paper, twocolumn]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{url}
\usepackage{color}
\usepackage{xspace} 		% allows space to come after a command
\usepackage{graphicx}
\graphicspath{{figs/}}
\usepackage{booktabs}
\usepackage{fullpage}
\usepackage{bibentry}  % to allow inline bibliogrtaphies

\definecolor{red}{rgb}{0.95,0.4,0.4}
\definecolor{blue}{rgb}{0.4,0.4,0.95}

\newcommand{\todo}[1]{\textcolor{red}{TODO: #1}}
\newcommand{\note}[1]{\textcolor{blue}{NOTE: #1}}

\newcommand*{\eg}{e.g.\@\xspace}
\newcommand*{\ie}{i.e.\@\xspace}
\newcommand*{\ea}{et al.\@\xspace}
\newcommand*{\fig}{Figure\@\xspace}

\newcommand{\fv}{\mathbf{x}}
\newcommand{\fx}{\mathbf{x}_1}
\newcommand{\fy}{\mathbf{x}_2}
\newcommand{\elx}{x_{1i}}
\newcommand{\ely}{x_{2i}}
\newcommand{\prob}{Pr}
\newcommand{\labelvec}{\mathcal{Y}}
\newcommand{\olabel}{y}
\newcommand{\obj}{\mathcal{O}}
\newcommand{\nitems}{M}
\newcommand{\nclusters}{K}

\newcommand{\affmatrix}{W}
\newcommand{\affentry}{w}

\newcommand{\realnumbers}{\mathbb{R}}
\newcommand{\integers}{\mathbb{Z}}
\DeclareMathOperator*{\argmin}{arg\,min}

\usepackage{algorithm}
\usepackage[noend]{algorithmic}
%\newcommand{\localfv}{\fv^{local}}
%\newcommand{\neighbourhood}{\mathcal{N}}
%\newcommand{\regionfvcreation}{\phi}
%\newcommand{\pairfvcreation}{\Phi}

\title{Supervised Correlation Clustering: A Tutorial}
\author{Michael Firman}



\begin{document}

\maketitle
\begin{abstract}
In this document we explore correlation clustering in a digestible format, such that it is easily accessible to anyone with minimal experience in mathematics and computer science. We motivate correlation clustering as a vehicle for supervised clustering, where exemplar clusterings can be used to teach a system how data should be grouped. We offer a pipeline for such a system, using a Random Forest classifier to infer pairwise measures, and we explore alternatives for inferring the final labelling. We review the literature as we go, highlighting important contributions in the field and augmenting them with novel developments.
\end{abstract}



\section{Introduction}
\label{sec:intro}

\begin{figure*}
	\centering
	\includegraphics[width=2.0\columnwidth]{clustering_options.pdf}
	\caption{Looking at different properties can give very different clusterings.}
	\label{fig:different_clusterings}
\end{figure*}

The goal of clustering is to assign a label $y_i$ to each item in a set of $M$ distinct items $\{\obj_i\}^M_{i=1}$, such that similar items have identical labels while dissimilar items have different labels. % \cite{Zhu2009}.
This objective raises the obvious question of what we mean by the terms  \emph{similar} and \emph{differing}. Looking at different aspects of objects' properties may give different clusterings, and for each property there are various degrees of `similarity' (\fig~\ref{fig:different_clusterings}).
In fact, many clustering methods avoid ever formally defining these measures, and instead rely on good input data and one or more user specified parameters in order to constrain the problem. These parameters may be the number of clusters required (\eg $k$-means \cite{macqueen-bsmsp-1967} or spectral clustering \cite{ng-nips-2001}) or another measure (\eg datapoint `self-similarity' for affinity propagation \cite{frey-science-2007}).
%, and also topic modelling approaches such as latent Dirichlet allocation \cite{Blei2003}. Other clustering methods use alternative parameters; mean-shift \cite{Fukunaga1975} requires a user-specified kernel function, and affinity propagation \cite{Frey2007} requires a parameter for each data point, specifying its likelihood to be chosen as a cluster centre.



%Even clustering methods which automatically estimate the number of classes, such as $x$-means have drawbacks.
Typically, each item $\obj_i$ is represented by a feature vector $\fv_i \in \realnumbers^{(D\times1)}$, where each dimension of $\fv_i$ is an individual \emph{feature} --- \ie a numerical measure of a single property of $\obj_i$. Clustering in Euclidean feature space is highly sensitive to the scaling of each such feature; \eg a feature in the range $[0, 200]$ would have a far larger influence than one in the range $[0, 0.1]$.
Even after suitable scaling of features, most clustering algorithms treat each feature as being equally important, when in fact it is likely that there are some features which are far more important than others for the task in hand. This makes distances in Euclidean space poorly suited to matching feature vectors.
Finally, most clustering algorithms are very sensitive to the inclusion of singleton data points, which do not belong in any clusters.
While attempts have been made to modify existing clustering algorithms to deal with singleton `noisy' data points (\eg \cite{}), these require additional user parameters to be set and suffer from the same problems already outlined.
%\todo{Give couple of examples of where problem has been tried to be solved}.



\subsection{Correlation Clustering}


Correlation clustering describes a family of clustering algorithms which rely on pairwise similarities between data points to both find optimal groupings and to estimate the number of clusters \cite{bansal-ml-2002}.
Given a symmetric affinity matrix $\affmatrix$, where each entry $\affentry_{ij} \in \realnumbers$ represents a degree of attraction ($+$ve values) or repulsion ($-$ve values) between data points $i$ and $j$, a correlation clustering algorithm finds a labelling of the data points $\labelvec = \{\olabel_1, \dots, \olabel_\nitems\}$ which minimizes the energy function
\begin{equation}
	E(\labelvec) = - \sum_{i=1}^\nitems \sum_{j=1}^\nitems \affentry_{ij} \, [ \olabel_i = \olabel_j ],
\label{eqn:corr_clust_energy}
\end{equation}
where $[\bullet]$ is the Iverson bracket, which equals 1 when the statement inside it is true and 0 otherwise.
Minimising (\ref{eqn:corr_clust_energy}) is equivalent to maximising the sum of the within-cluster edges. It is also inherently minimising the sum of the between-cluster edges
\begin{equation}
	E^*(\labelvec) = \sum_{i=1}^\nitems \sum_{j=1}^\nitems \affentry_{ij} \, [\olabel_i \neq \olabel_j].
\end{equation}
This is apparent when it is realised that the sum of all the edges $\sum_{ij} \affentry_{ij}$ is fixed. The aim of a correlation clustering solver is to discover a labelling $\labelvec^*$ which minimises (\ref{eqn:corr_clust_energy}). We discuss such solvers in section \ref{sec:solvers}, but first we discuss methods for constructing the affinity matrix $\affmatrix$.

If the weights in $\affmatrix$ are restricted to the binary choice of $\{-1, +1\}$, then the optimal solution $\labelvec^*$ is the one that minimises the disagreements.

%The pairwise formulation of correlation clustering allows the limitations associated with traditional clustering algorithms to be overcome.
In contrast to distance measures in typical clustering algorithms, the entries in $\affmatrix$ can specify a desire to place two objects in the same cluster (when $\affentry < 0$), as well as in differing clusters. The entries also need not satisfy the triangle inequality.

%We discuss methods for recovering $\labelvec$

\subsection{When to use correlation clustering}
Correlation clustering is particularly well suited to some problems, but not to others. The problem area in which correlation clustering can be seen to be the most useful is when there exists a pairwise classifier which can specify if two objects should be placed in the same cluster or not. It is particularly useful when the number of clusters is not known.

Advantages and disadvantages over other clustering solvers:
\paragraph{Advantages}
- No need to specify number of clusters or other such parameters
- Can train on exemplar clusterings
- Simple interpretation
- Fully probablistic
- Variety of solvers available

\paragraph{Disadvantages}
- Larger storage space and running time required than many other clustering algorithms
- Requires a pairwise metric
- Pairwise metric has distinct `pivot point' - effectively a parameter (see section)


\subsection{Correlation clustering pipeline image}

\section{Computing pairwise affinities}

For correlation clustering, we require pairwise affinities between data points, such that negative affinities imply the objects should take on different labels, and positive affinities imply the objects should take on the same label:
\begin{equation}
\affentry_{ij}
\left\{
	\begin{array}{lll}
		 > 0  & \implies & y_i = y_j \\
		 = 0  & \implies & \texttt{No information} \\
		 < 0  & \implies  & y_i \neq y_j
	\end{array}
\right.
\label{eqn:w_measures}
\end{equation}


It is proved in \cite{bagon-corr-2011} that the labelling $\labelvec^*$ is the maximum-likelihood labelling, assuming the affinities in $\affmatrix$ are interpreted as log-odds ratios:
\begin{eqnarray}
\affentry_{ij} &= &\log\left(\frac{p_{ij}}{1-p_{ij}}\right),
\label{eqn:log_odds}
\end{eqnarray}
where $p_{ij} \equiv \prob([\olabel_i = \olabel_j])$ is the probability that data points $\obj_i$ and $\obj_j$ should take on the same class label. Elsner and Schudy \cite{elsner-ilpnlp-2009} denounce the logarithmic conversion as having a ``tenuous mathematical justification'', since it assumes $p_{ij}$ are independent and identically distributed --- an assumption that will rarely hold true in practice.

%that minimises (\ref{eqn:corr_clust_energy})

\begin{table*}
	\footnotesize
	\centering
	\begin{tabular}{p{6cm}p{6cm}}
	\toprule
	\textbf{k-means}  &	\textbf{Correlation clustering} \\
	\midrule
	Geometric; minimises sum of Euclidean distances &
	Probabilistic; maximises overall likelihood  \\
	\midrule
	Number of clusters specified and fixed &
	Clusters can be created and destroyed during solving routine \\
	\midrule
	Clusters represented by cluster center point &
	Clusters represented by the points and edges within the cluster \\
	\midrule
	Iterative greedy solver, assigning objects in turn to `best' cluster &
	Several solvers available - including iterative greedy solver \mbox{AL-ICM}.	\\
	\bottomrule
	\end{tabular}
	\caption{Alternative functions for forming $W\in[-\infty,+\infty]$ from various domains of input $x$. All functions are monotonically increasing over the domain of $x$.}
	\label{tab:kmeans_comparison}
\end{table*}

\paragraph{Pairwise classification}

%We outlined in Section \ref{sec:intro} the issues associated with distance measures in Euclidean space, which are used by many clustering algorithms (e.g....).
%For correlation clustering, it instead makes sense to treat each pairwise measure as a two-class classification problem, such that the classifier assigns each pair to a class:
%\begin{equation}
%f : (\fx, \fy) \rightarrow \{\olabel_i=\olabel_j, \, \olabel_i \neq \olabel_j\}.
%\end{equation}
%These class memberships can be converted to scores in $\{-1, +1\}$ using the formula

\todo{Talk about supervised classification - where training data comes from}

For correlation clustering, it makes sense to treat each pairwise measure as a two-class classification problem, such that the classifier assigns to each pair the probability $p_{ij} \equiv \prob([\olabel_i = \olabel_j])$ of the event that the pair take on the same label
%:
%\begin{equation}
%f : (\fx, \fy) \rightarrow \prob([\olabel_i = \olabel_j]).
%\end{equation}
%For ease of notation, we define $p_{ij} .
The classifier can be trained using pairs from exemplar clusterings.
%At test time the classifier can be used to infer $p_{ij}$ for new unlabelled pairs of objects $(\obj_i, \obj_j)$.
Such \emph{equivalence constraint learning} is a fairly widely studied problem in its own right, and there is a pool of generative \cite{} and discriminative \cite{} techniques which could be exploited.

%\subsection{Discriminative approaches}
For the purposes of this tutorial we outline a discriminative approach using a Random Forest classifier.

\begin{figure*}
	\centering
	\includegraphics[width=2.0\columnwidth]{rf_overview.pdf}
	\caption{A possible pipeline for supervised computation of pairwise affinities.}
	\label{fig:rf_overview}
\end{figure*}

\paragraph{Pairwise feature vector}
Each pair of objects has an associated pair of feature vectors $(\fv_i, \fv_j)$. Most common classification systems require a single feature vector. We require a pairwise mapping function to convert the pair of feature vectors into a single feature vector. %$\phi: (\fv_i, \fv_j) \rightarrow \fv_{ij}$
One option for $\phi$ is a component-wise absolute difference function
\begin{eqnarray}
  \fv_{ij} & = & | \fv_i - \fv_j |.
\end{eqnarray}
Each $\fv_{ij}$ is of the same dimension as $\fv_i$; each element of $\fv_{ij}$ captures a difference in one feature dimension.
This absolute difference function has the convenient properties of being symmetric and having a simple physical interpretation.
%Other alternatives for the pairwise mapping are the asymmetric function
%\begin{eqnarray}
%  \fv_{ij} & = & \fv_i - \fv_j
%\end{eqnarray}
An alternative pairwise mapping, suggested by \cite{xiong-kddm-2012}, is the position-dependant function
\begin{eqnarray}
  \fv_{ij} & = & \left[
\begin{array}{c}
  |\fv_i - \fv_j| \\
  \frac{1}{2}(\fv_i + \fv_j)
\end{array}
  \right].
\end{eqnarray}
On small to medium volumes of training data, we have found this function to suffer from over-fitting.

Given each $\fv_{ij}$, we then wish to estimate $\prob([\olabel_i = \olabel_j])$.
This two-class classification problem is one of the oldest and best-explored examples of machine learning. There are many algorithms which could be used, such as support vector machines, logistic regression or nearest neighbour classification. One option we present here is with a \emph{Random Forest}.
% the probability that $\obj_i$ and $\obj_j$ should be assigned the same label, which we denote as $p_{ij} =



\newcommand{\tree}{T}
\newcommand{\trees}{\mathcal{T}}

\paragraph{Estimating $p_{ij}$ with a Random Forest}
Random Forests are well-suited to this probablistic classification problem. A Random Forest is a set of decision trees $\trees = \{\tree\}$, each of which is trained on a different subset of the training data \cite{Breiman2001}.
At test time, each data point is passed through each tree, and at each node is passed left or right depending on the feature thresholds learned at training time. The final leaf node the data point ends in will vote for a class, based on which training samples ended up at that leaf node.
The class predicted by a single tree for feature vector $\fv_{ij}$ can be written as $\tree(\fv_{ij})$.

%Random Forests were used for such `classification distance learning' by Xiong \ea \cite{xiong-kddm-2012}, who found them to outperform the widely used Mahalanobis metric.
They are well suited to this purpose, as they automatically select the features most suitable for separating the classes (unlike \eg SVMs) and they examine each dimension separately, making them robust to different scaling of each feature. They are also inherently non-linear, perform quick training and inference and are easily parallelizable.

Often the modal class vote is taken as a hard prediction of class membership. For use in correlation clustering it is possible to instead to approximate a probability of class membership using the fraction of votes for each class \cite{bostrom-mla-2007, firman-iros-2013}. We can therefore make the estimation
\begin{eqnarray}
p_{ij} & = & \frac{\sum_{\tree \in \trees} [\tree(\fv_{ij}) = 1]}{|\trees|}
\label{eqn:forest_voting}
\end{eqnarray}
%$p_{ij}$ can be seen as the parameter of a Bernoulli distribution; \ie there is a probability $p_{ij}$ that $\obj_i$ and $\obj_j$ belong to the same class.
% p_{ij} & = & \frac{\sum_{t=1}^{N_t} [T_t(\fv_{ij}) = 1]}{N_t}


\paragraph{Laplace smoothing for probability predictions}
When using (\ref{eqn:log_odds}) to form the mapping, and $x\in\{0, 1\}$, $\affentry$ will map to $\pm\infty$. This causes issues in the evaluation of (\ref{eqn:corr_clust_energy}) as it becomes hard to compare two alternative clusterings when the energy term is saturated by infinite values (figure \ref{fig:comparing_clusterings}). To counter this, it can be wise to smooth the probability estimations using Laplacian smoothing, \ie
\begin{eqnarray}
p_{ij} & = & \frac{\sum_{t=1}^{N_t} [T_t(\fv_{ij}) = 1] + \alpha}{N_t + 2\alpha}.
\end{eqnarray}
Setting $\alpha>0$ ensures that $0<p_{ij}<1$, and hence $\affentry$ is finite.

As an alternative, $\affentry_{ij}$ can be calculated using (\ref{eqn:forest_voting}) and capped to a large finite value $\pm\eta$. To avoid overflow it is prudent to set $\eta \le \texttt{MAX}/M^2$, where $\texttt{MAX}$ is the largest number representable by the system being used.





\paragraph{Class imbalance in training}
With a discriminative classifier such as a Random Forest, it is important to deal with the class imbalance in the training data. Hammer \ea \cite{hammer-cp-2009} showed that when it comes to equivalence constraints, there are approximately $K$ times more negative constraints than positive ones, \ie $\sum_{ij} [\olabel_i \ne \olabel_j] \approx \nclusters \sum_{ij} [\olabel_i = \olabel_j]$.
The large class imbalance when $\nclusters$ is high can have detrimental effects on the performance of the Random Forest classifier, and can cause a skew in the results. To address this issue one can choose an equal sized random subsample of positive and negative pairs to be passed to the classifier for training.
\todo{ref that paper which talked about this}


\subsection{A prior on cluster size}

Bagon and Galun \cite{bagon-arxiv-2012} show that as the correlation clustering solver places a uniform prior over all possible partitions, this induces a prior on the number of classes $k$ of roughly $\frac{n}{\log n}$.

Alush and Goldberger \cite{alush-simbad-2013} show that a prior probability $q$ that two points should fall into the same cluster can be introduced, modifying (\ref{eqn:log_odds}) like so:
\begin{equation}
W_{ij} = \log\left(\frac{p_{ij}}{1-p_{ij}}\right) +
\log\left(\frac{q}{1-q}\right).
\end{equation}



\section{Correlation clustering solvers}
\label{sec:solvers}

Elsner and Schudy \cite{elsner-ilpnlp-2009} empirically evaluate a variety of different correlation clustering solvers on
Furthermore, they show a method of computing a bound on $E(\labelvec)$ using a semi-definite programming relaxation.

Alush and Goldberger note that a pre-processing step can be introduced to any solver, diving the points into ...

NP-Hard


Include table of different solvers, strengths weaknesses etc.


Given a distance measure between each pair of objects, it is then tempting to place pairs of objects which are similar into the same cluster, and dissimilar objects in different clusters. Depending on the choice of distance measure, different problems will arise. Using $L_p$ and even Mahalanobis distances have the problem that it is still hard to know how to define two items as being in the same group vs different groupings.
With a classification-based pairwise measure a different problem arises, that it is very likely that many of the distance measures do not obey the triangle inequality. Imagine a scenario with three items, $A$, $B$ and $C$. If pairs $(A, B)$ and $(B, C)$ are found to be similar while $(A, C)$ are found to be dissimilar, it is difficult to know how to optimally assign labels.

Bansal \ea \cite{bansal-ml-2002} introduced the correlation clustering problem in 2004, and initially focussed on solving problems with binary edge weights $\in \{-1, 1\}$ using the principle of minimising the number of disagreeing triangles of edges.
Bagon and Galun \cite{bagon-corr-2011}  introduced two novel solvers for continuous edge weights based on an extension of the graph-cuts algorithm \cite{rother-cvpr-2007}, and a third iterative solving method which greedily swaps points between clusters until the energy cannot be lowered any further.

\subsection{The AL-ICM solver}

Based on earlier work by Besag \cite{besag-jrss-1986}, \mbox{AL-ICM} is a greedy iterative solver which changes the label of each $y_i$ in turn to the label which it is most attracted to, \ie which minimises the energy the most. If the data point is repelled by all clusters, then it is assigned a unique singleton label. This is outlined in algorithm \ref{alg:al_icm}.


\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

\begin{algorithm}
\begin{algorithmic}[H]
\REQUIRE $\affmatrix \in \realnumbers^{N \times N}$, $\labelvec_\texttt{guess} \in \integers^{1 \times N}$
\ENSURE $\labelvec \in \integers^{1 \times N}$
\STATE $\labelvec \leftarrow \labelvec_\texttt{guess}$
\STATE $\labelvec_\texttt{last} \leftarrow []$
\WHILE {$E(\labelvec) < E(\labelvec_\texttt{last})$}
\STATE $\labelvec_\texttt{last} \leftarrow \labelvec$
\FOR[Loop over each data point]{each $\olabel_i$ in $\labelvec$}
	\STATE $\mathbf{k} \leftarrow \mathbf{0}_{\max(\labelvec)\times1}$
	\FOR[Sum $y_i$'s attraction to each existing cluster] {each $\olabel_j$ in $\labelvec$}
	\STATE $\mathbf{k}_{y_j} \leftarrow \mathbf{k}_{y_j} + \affentry_{i,j}$
	\ENDFOR
	\IF {$\min( \mathbf{k}) < 0$}
		\STATE $\olabel_i \leftarrow \max(\labelvec) + 1$ \COMMENT{Create new singleton cluster}
	\ELSE
		\STATE $\olabel_i \leftarrow \underset{j}{\argmin} \, \mathbf{k}_j$ \COMMENT{Assign to most attracted cluster}
	\ENDIF
\ENDFOR
\ENDWHILE
\end{algorithmic}
 \caption{AL-ICM correlation clustering solver \todo{Change to be more mathematical}}
 \label{alg:al_icm}
 \end{algorithm}

Clearly each iteration of this algorithm is of order $O(N^2)$; for each of the $N$ points, each other point is summed over to compute the point-to-cluster score vector $\mathbf{k}$. %The overall complexity of such an iterative algorithm is hard to judge
The algorithm does not guarantee convergence and as a greedy solver it is prone to local minima. The initial guess of the labels can therefore have an effect on the final solution.
In the original implementation\footnote{Available from \url{http://www.wisdom.weizmann.ac.il/~bagon/matlab.html}} the initial guess of the labels $\labelvec_\texttt{guess}$ is set such that each item is given the same label. To avoid problems with local minima in this greedy solver we run the algorithm with multiple different values for $\labelvec_\texttt{guess}$ and choose the clustering which gives the lowest energy (equation \ref{eqn:corr_energy_2}).
For our values of $\labelvec_\texttt{guess}$ we choose a vector of all ones (\ie a single cluster), a vector of unique numbers (each object in a different cluster and four more vectors of randomly assigned integers $\in [1, N]$).


\todo{Talk about each iteration in terms of maximising the probability}

Introduce solver here

\paragraph{Initialisation}

The MATLAB AL-ICM code runs the solver only once, with an initial labelling of each data having a unique, singleton label. I made a modification whereby the solver is run multiple times, with different initial labellings. I used: 1) All items in same cluster, 2) all items in separate clusters, and 3-8) choose a number of clusters at random, then assign labels randomly. This helps to prevent local minima, although the initial labelling (2) did seem to win out a lot of the time.

\paragraph{Stopping criteria}
As a greedy solver, the iterations stop when the energy has not been lowered any further.



\paragraph{Similarity to k-means}

I am interested by the parallels between the AL-ICM solver and k-means.  Both work by looping over each data point, and assigning it to the cluster which minimises the energy the most. In k-means, the number of clusters is specified and fixed, while in correlation clustering cluster labels can be created and destroyed during the solving routine. In k-means, at the end of this iteration the cluster 'centres' are updated, while in correlation clustering the clusters do not have 'centres', and are instead represented abstractly by the pairwise measures between each of the items in the cluster and all the other items in the dataset.

The `distance' from a point to cluster in $k$-means is a distance in $L_2$ space, while in \mbox{AL-ICM} it is the sum of all the edges from the point to each point in the cluster.
Our use of multiple starting guesses for \mbox{AL-ICM} is inspired by the analogous multiple replicate version of $k$-means.

%\paragraph{Simulated annealing}

%One extension to k-means to avoid local minima is to use simulated annealing, where the energy is permitted to rise by a small amount, particularly early on in the solving process. I have not attempted this in correlation clustering. In my experience it is really the pairwise scores which have a bigger effect on the quality of the clustering, rather than the solving routine.

\paragraph{Complexity and storage}

If all pairwise terms are computed between the N items, then $O(N^2)$ storage is required. Each iteration of the solver is $O(N^2)$, as each data point is looped over and compared to each other data point. I haven't managed to get a handle on the average/worst case complexities for the overall solving process, but in my experience it is roughly $O(N^2 \log(N) )$. Again, looking at the parallels with k-means may turn up some relevant literature in this field. Using a sparse set of pairwise measures obviously decreases these, but I have never tried this - I don't know what the drop in performance is.

\section{Additional information}

\subsection{Alternatives to correlation clustering}

If the number of data points is very high, then it may be the case that a more efficient clustering method is required. In computer vision, sometimes a graph-based community discovery algorithm is employed. This requires unweighted edges between nodes - so the graph could be formed by placing a threshold on the pairwise terms, and only retaining edges above the threshold. I haven't used these but one method is [1], which was used to discover objects by [2].


This problem bears strong similarities to the widely explored problem of finding communities in networks, and several clustering algorithms have made use of community detection schemes to find their clusterings---for example, Kang \ea \cite{kang-iccv-2011} use a community detection algorithm introduced by Tyler \ea \cite{tyler-ctbook-2003} to find repeated object instances in photo collections. While community detection schemes are usually highly scalable, they often require user parameters and many do not allow for weighted edges.


\subsection{Comparing partitions}

The Rand Index (\url{http://en.wikipedia.org/wiki/Rand_index}) is a simple way of comparing two partitionings, useful if you have a ground truth partitioning you want to compare to an experimental result.

\subsection{Alternative ways of forming the }

\begin{table*}
	\footnotesize
	\centering

	\begin{tabular}{lllp{6cm}}
	\toprule
	\textbf{Domain}  &	\textbf{$f$} & \textbf{Codomain} & \textbf{Notes} \\
	\midrule
	$\{0,1\}$ &
	$2x-1$  &
	$\{-1, 1\}$ &
	Assuming $x=[y_i=y_j]$.\\
	\midrule
	$[0,1]$ &
	$\log\left(\frac{x}{1-x}\right)$   &
	$[-\infty, +\infty]$  &
	Inverse sigmoid function. $x=0.5$ is the pivot point. Useful when $x$ is a probability or a cosine similarity.\\
	\midrule
	$[0,t]$ &
	$\log\left(\frac{x}{t-x}\right)$ &
	$[-\infty, +\infty]$  &
	Scaled inverse sigmoid function. $x=\frac{t}{2}$ is the pivot point. Values of $x$ greater than $t$ map to $\infty$.\\
	\midrule
	$[0,\infty]$ &
	$\log(tx)$ &
	$[-\infty, +\infty]$  &
	Logarithmic function. $x=\frac{1}{t}$ is the pivot point.\\
	\bottomrule
	\end{tabular}
	\caption{Alternative functions for forming $W\in[-\infty,+\infty]$ from various domains of input $x$. All functions are monotonically increasing over the domain of $x$.}
	\label{tab:mapping_functions}
\end{table*}

\section{Applications of correlation clustering}
Most clustering algorithms use a distance metric to measures degree of dissimilarity between data points $\fx$ and $\fy$. One of the most common families of distance metrics are $L_p$ measures $d_p(\bullet, \bullet)$
%where
%\begin{equation}
%d_p( \fx, \fy ) = \sqrt[p]{ \sum_{i = 1}^{n} | \elx - \ely |^p }
%\end{equation}
such as the Euclidean distance ($p=2$) and Manhattan distance ($p=1$).
While such metrics in $\realnumbers^{+}$ can be converted to span the full range specified by (\ref{eqn:w_measures}) (see table \ref{tab:mapping_functions}), this requires the user to specify a parameter $t$, which becomes the pivot point; values of $x$ above $\frac{1}{t}$ map to positive values of $\affentry$, while those below $\frac{1}{t}$ map to negative values of $\affentry$. Furthermore, $L_p$ distances suffer from the problems with feature scaling and importance outlined in section \ref{sec:intro}, and they only provide a measure of \emph{dissimilarity}, while for correlation clustering we also want to capture \emph{similarity} between data point.

%A new opportunity for distance assessment is opened up if training data is available.
%Kostinger \ea \cite{kostinger-cvpr-2012} learn a Mahalanobis metric to find equivalence constraints between data points; this metric uses a learned matrix $\mathbf{S}$ to warp the metric space to respect the training data distribution. The distance is then computed as
%\begin{equation}
%d_{Mahalanobis}( \fx, \fy ) = \sqrt{(\fx-\fy)^{T} \mathbf{S}^{-1} (\fx-\fy)}.
%\end{equation}

%Bagon \ea \cite{bagon-corr-2011} also use a Mahalanobis metric.

%All these distance measures so far . In our work we don't just want to learn when objects are dissimilar, we want to also learn when they are similar.

\section{Key Correlation Clustering Works}

\nobibliography{../bibtex/strings.bib,../bibtex/main.bib,../bibtex/crossrefs.bib}
\begin{itemize}
\item
\bibentry{bansal-ml-2002}
\item
\bibentry{bagon-corr-2011}
\item \bibentry{alush-simbad-2013}
\item \bibentry{elsner-ilpnlp-2009}
\end{itemize}
%Thorsten Joachims and John Hopcroft. Error bounds for correlation clustering.
%http://cs.brown.edu/people/claire/Publis/corrclustering.pdf -> make the stupid assumption

{
\footnotesize
\bibliographystyle{plain}
\bibliography{../../bibtex/strings.bib,../../bibtex/main.bib,../../bibtex/crossrefs.bib}
}

\end{document}



